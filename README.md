# OpenGL Skeletal Animation

Framework: OpenGL 4.5\
Platform: Linux

## Features
- Grid
- Skeletial animation (Assimp, interpolation between keyframes)
- 3D text (freetype)
- Resource manager (with hashing)
- Quaternion camera
- Third person camera

## Controls
- Arrow keys - Control the spider
- W/S - Camera forward/backward (Free camera mode)
- A/D - Camera left/right (Free camera mode)
- Q/E - Camera roll ccw/cw (Free camera mode)
- 1 - Toggle wireframe
- 2 - Next animation
- 3 - Play/pause animation
- 4 - Toggle third person camera

## Demo
[Video](https://www.youtube.com/watch?v=t3XSclcIS0g)

