#version 450

layout(location=0) in vec3 position;
layout(location=1) in vec2 uv;
layout(location=0) uniform mat4 proj;
layout(location=1) uniform mat4 view;
layout(location=2) uniform mat4 model;
layout(location=0) out vec2 fragUv;

void main()
{
    gl_Position = proj * view * model * vec4(position,1.0);
    fragUv = uv;
}