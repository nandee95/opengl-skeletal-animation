#version 450

#define MAX_BONES 100
#define MAX_BONES_PER_VERTEX 8

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;
layout(location = 3) in int boneId[MAX_BONES_PER_VERTEX];
layout(location = 4) in float boneWeight[MAX_BONES_PER_VERTEX];

layout(location = 0) uniform mat4 model;
layout(location = 4) uniform mat4 bones[MAX_BONES];
layout(std140,binding=0) uniform baseMatrices
{
    uniform mat4 projView;
}; 

layout(location = 0) out vec2 fragUv;

void main()
{    
    mat4 bone=mat4(0.0);
    for(int i=0;i<MAX_BONES_PER_VERTEX && boneId[i] != -1;i++)
    {
        bone += bones[boneId[i]] * boneWeight[i];
    }

    gl_Position = projView * model * bone *  vec4(position,1);
    fragUv = uv;
}