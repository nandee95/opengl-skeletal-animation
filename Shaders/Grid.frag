#version 450

layout(location = 0) out vec4 FragColor;
layout(location = 0) uniform vec4 attrColor;


float near = 0.1; 
float far  = 1e6; 
  
float LinearizeDepth(float depth) 
{
    float z = depth * 2.0 - 1.0; // back to NDC 
    return (2.0 * near * far) / (far + near - z * (far - near));	
}

void main()
{
    const float depth = LinearizeDepth(gl_FragCoord.z) / 5000;
    if(depth > 1) discard;
    FragColor = attrColor * (1 - depth);
}