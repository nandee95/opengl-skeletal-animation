#version 450
layout(location = 0) out vec2 fragUv;

const vec2 pos[4] = vec2[4] (
	vec2(-1,  1),
	vec2(-1, -1),
	vec2( 1,  1),
	vec2( 1, -1)
);

const vec2 uv[4] = vec2[4] (
	vec2(0, 1), 
	vec2(0, 0),
	vec2(1, 1),
	vec2(1, 0)
); 


void main()
{
    gl_Position = vec4(pos[gl_VertexID],0,1);
	fragUv = uv[gl_VertexID];
}