#version 450
#pragma optionNV (unroll all)

layout(location = 0) in vec2 uv; 
layout(location = 0) uniform sampler2D scene; 
layout(location = 0) out vec4 outColor;

void main() {
    outColor = texture(scene,uv);
}