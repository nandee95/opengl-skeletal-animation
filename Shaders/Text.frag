#version 450

layout(location = 0) out vec4 FragColor;
layout(location = 0) in vec2 uv;
layout(location = 1) uniform sampler2D font;
layout(location = 2) uniform vec4 color;

void main()
{
    const float val = texture(font,uv).r;
    if(val<0.5) discard;
    FragColor = color;
    //gl_FragDepth = 0;
}