#version 450

layout(location=0) in vec2 position;
layout(location=1) in vec2 uv;

layout(location=0) uniform mat4 model;

layout(location=0) out vec2 fragUv;

layout(std140,binding=0) uniform baseMatrices
{
    uniform mat4 projView;
}; 


void main()
{
    gl_Position = projView * model * vec4(position,0.0,1.0);
    fragUv = uv;
}