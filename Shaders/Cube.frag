#version 450

layout(location = 0) out vec4 FragColor;
layout(location = 0) in vec2 uv;
layout(location = 3) uniform sampler2D diffuse;

void main()
{
    FragColor = vec4(texture(diffuse,uv).rgb,1);
}