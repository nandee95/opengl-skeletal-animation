#version 450

layout(location=0) in vec3 position;

layout(std140,binding=0) uniform baseMatrices
{
    uniform mat4 projView;
}; 

void main()
{
    gl_Position = projView * vec4(position,1.0);
}