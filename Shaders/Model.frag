#version 450

layout(location = 0) in vec2 uv;
layout(location = 1) uniform sampler2D diffuseTex;
layout(location = 2) uniform bool blackKeyed;
layout(location = 3) uniform bool hasTexture;
layout(location = 0) out vec4 FragColor;
layout(location = 1) out vec4 BloomColor;


void main()
{
    if(!hasTexture)
    {
        FragColor = vec4(0.0,0.0,0.0,1);
        return;
    }
    FragColor = vec4(texture(diffuseTex,uv).rgb,1);
    if(FragColor.rgb == vec3(1,1,1)) discard;
    if(blackKeyed)
        FragColor.a = (FragColor.r+FragColor.g+FragColor.b)/3;
}