# Setup
cmake_minimum_required(VERSION 3.13 FATAL_ERROR)
include("Scripts/ExceptionTable.cmake")

# Create project
set(APP_NAME OpenglAnim)
project(${APP_NAME} CXX)

# Set directories
set(PROJECT_CODE_DIR ${PROJECT_SOURCE_DIR}/Source)

file(GLOB_RECURSE APP_SOURCES ${PROJECT_CODE_DIR}/*.cpp)
file(GLOB_RECURSE APP_HEADERS ${PROJECT_CODE_DIR}/*.hpp)

create_exception_table("${PROJECT_SOURCE_DIR}/Source/Generated/ExceptionTable.enum" ${APP_HEADERS})

find_package(OpenGL REQUIRED)

if(${OpenGL_OpenGL_FOUND})
    list(APPEND APP_LIB OpenGL::GL OpenGL::GLX)
endif()

find_package(GLEW REQUIRED)
if (GLEW_FOUND)
    include_directories(${GLEW_INCLUDE_DIRS})
    link_libraries(${GLEW_LIBRARIES})
endif()

find_package(ASSIMP REQUIRED)
if (ASSIMP_FOUND)
    include_directories(${ASSIMP_INCLUDE_DIRS})
    link_libraries(${ASSIMP_LIBRARIES})
endif()

find_package(Freetype REQUIRED)

if(FREETYPE_FOUND)
    include_directories(${FREETYPE_INCLUDE_DIRS})
    link_libraries(${FREETYPE_LIBRARIES})
endif()


find_package(PkgConfig REQUIRED)
pkg_search_module(GLFW REQUIRED glfw3)
list(APPEND APP_INC ${GLFW_INCLUDE_DIRS})
list(APPEND APP_LIB ${GLFW_LIBRARIES})

add_executable(${APP_NAME} ${APP_SOURCES})
target_include_directories(${APP_NAME} PUBLIC ${APP_INC})
target_link_libraries(${APP_NAME} ${APP_LIB})
target_compile_features(${APP_NAME} PRIVATE cxx_std_17)

set_target_properties(
	${APP_NAME}
	PROPERTIES
	RUNTIME_OUTPUT_DIRECTORY "${PROJECT_SOURCE_DIR}/Build"
	OUTPUT_NAME ${APP_NAME}
	LINKER_LANGUAGE CXX
)