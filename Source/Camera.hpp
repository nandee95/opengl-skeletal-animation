#pragma once

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>

class Camera
{
    float aspectRatio = 1.f;
    float fov = glm::radians(90.f);
    glm::vec3 position={0,0,0};
    glm::quat orientation={1,0,0,0};
public:
	inline const void SetPositon(const glm::vec3& position)
	{
		this->position = position;
	}

	inline const glm::vec3& GetPosition() const
	{
		return position;
	}

	inline void SetOrientation(const glm::quat orientation)
	{
		this->orientation = orientation;
	}

	inline const glm::quat& GetOrientation() const
	{
		return orientation;
	}

    inline void SetFOV(const float& fov)
    {
        this->fov = fov;
    }

    inline const float& GetFOV() const
    {
        return fov;
    }

    inline void SetAspectRatio(const float& aspectRatio)
    {
        this->aspectRatio = aspectRatio;
    }

    inline const float& GetSetAspectRatio() const
    {
        return aspectRatio;
    }
	
	inline const void Turn(glm::vec3 angle)
	{
		orientation = glm::rotate(glm::normalize(orientation), angle.x, glm::vec3(0, 0, 1) * orientation);
		orientation = glm::rotate(glm::normalize(orientation), angle.y, glm::vec3(0, 1, 0) * orientation);
		orientation = glm::rotate(glm::normalize(orientation), angle.z, glm::vec3(1, 0, 0) * orientation);
	}

	inline const void Move(glm::vec3 direction)
	{
		position += (glm::vec3(1, 0, 0) * orientation) * direction.x;
		position += (glm::vec3(0, 1, 0) * orientation) * direction.y;
		position += (glm::vec3(0, 0, 1) * orientation) * direction.z;
	}

	inline const glm::highp_mat4 CalcViewMatrix() const
	{
		const glm::vec3 eye = position;
		return glm::lookAt(eye, eye + (glm::vec3(1, 0, 0) * orientation), (glm::vec3(0, 0, 1) * orientation));
	}

	inline const glm::mat4 CalcProjMatrix(const float& nearPlane=0.1f,const float& farPlane = 1e6) const
	{
		return glm::perspective(fov, aspectRatio, nearPlane, farPlane);
	}
};