#pragma once

#include <glm/vec2.hpp>
#include <GL/glew.h>

#include "Texture.hpp"

class FrameBuffer
{
protected:
	GLuint FBO;
public:
	FrameBuffer(Texture2D& texture)
	{
		//Create FBO
		glGenFramebuffers(1,&FBO);
		Bind();

		//Binding texture to FBO
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture.GetId(), 0);

		Unbind();
	}

	inline void Bind()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, FBO);
	}

	static inline void Unbind()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	const GLuint& GetId()
	{
		return FBO;
	}

	~FrameBuffer()
	{
		glDeleteFramebuffers(1, &FBO);
	}
};
