#include "Model.hpp"


void Model::CreateFromFile(ResourceManager& resmg,const std::string filename)
{
    shader = std::make_shared<Shader>();

    rethrow(shader->CreateFromSourceFile("../Shaders/Model.vert","../Shaders/Model.frag"));

    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile(filename, aiProcess_Triangulate | aiProcess_FlipUVs );
    if (scene == nullptr  || !scene->mRootNode || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE)
        throw Exception(ExceptionType::ModelFailedToReadModelFile,"Failed to read model file: \v",filename);

    std::cout << "Loading model " << filename << std::endl;
    try
    {
        LoadMaterials(scene,resmg,std::filesystem::path(filename).remove_filename());
        LoadMeshes(scene);
        LoadAnimations(scene);
        LoadNode(rootNode,scene->mRootNode,scene);
    }
    catch(_Exception& e)
    {
        Destroy();
        throw e;
    }
    boneMatrices.resize(boneMap.size(),glm::mat4(1.f));
    boneMap.clear();
}