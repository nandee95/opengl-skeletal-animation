#pragma once

#include <memory>
#include <vector>

#include "Font.hpp"
#include "Entity.hpp"
#include "VertexArray.hpp"
#include "ResourceManager.hpp"
#include "Shader.hpp"

class Text : public Entity, public Colorable, public Transformable
{
    struct Vertex
    {
        glm::vec2 position;
        glm::vec2 uv;
    };
    VertexArray vao;
    std::shared_ptr<Shader> shader;
    std::shared_ptr<Font> font;
    uint32_t vertexCount =0;
    float width;
public:
    void Create(ResourceManager& resmg, const char* text)
    {
        rethrow(font = resmg.LoadResource<FontResource>({"../Resources/ace_futurism.ttf",{0,128}}));
        rethrow(shader = resmg.LoadResource<ShaderResource>({"../Shaders/Text.vert","../Shaders/Text.frag",ShaderSourceType::SourceFile}));

        SetText(text);
    }

    void SetText(const char* text)
    {
        std::vector<Vertex> vertices;
        glm::vec2 origin{0,0};
        Vertex corners[4];

        for(uint32_t i = 0;text[i] != '\0' ; i++)
        {
            if(static_cast<uint8_t>(text[i])>=128) continue;

            const auto& ch = font->GetCharacter(static_cast<uint8_t>(text[i]));

            corners[0] = {origin + ch.vtxBearing,ch.texOffset}; // Top left
            corners[2] = {corners[0].position + glm::vec2(ch.vtxSize.x, -ch.vtxSize.y ), corners[0].uv + ch.texSize}; // Bottom right
            corners[1] = {{corners[2].position.x, corners[0].position.y}, {corners[2].uv.x, corners[0].uv.y}}; // Top right
            corners[3] = {{corners[0].position.x, corners[2].position.y}, {corners[0].uv.x, corners[2].uv.y}}; // Bottom left

            origin.x += ch.advance;

            vertices.push_back(corners[0]);
            vertices.push_back(corners[2]);
            vertices.push_back(corners[1]);

            vertices.push_back(corners[0]);
            vertices.push_back(corners[3]);
            vertices.push_back(corners[2]);
        }

        width = origin.x;

        vertexCount = vertices.size();
		vao.FillVertices(vertices);
		vao.SetAttribPointers({{2,GL_FLOAT},{2,GL_FLOAT}});
    }

    virtual void Update() override
    {
        UpdateModelMatrix();
    }

    virtual void Draw() const
    {
        shader->Use();  
        shader->SetUniform(0,model);
        shader->SetUniform(1,10);
        shader->SetUniform(2,color);
        glActiveTexture(GL_TEXTURE10);
        font->BindTexture();
        vao.DrawArrays(GL_TRIANGLES,0,vertexCount);
    }

    const float& GetWidth() const noexcept
    {
        return width;
    }
};