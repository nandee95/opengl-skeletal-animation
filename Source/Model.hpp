
#include "Entity.hpp"
#include "Texture.hpp"
#include "Shader.hpp"
#include "Castings.hpp"
#include "Interpolation.hpp"
#include "UniformBuffer.hpp"
#include "ResourceManager.hpp"

#include <string>
#include <array>
#include <map>
#include <memory>
#include <iostream>
#include <chrono>
#include <filesystem>
#include <algorithm>
#include <cmath>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

REGISTER_EXCEPTION(ModelFailedToReadModelFile);
REGISTER_EXCEPTION(ModelNoExtensionFoundForTexture);
REGISTER_EXCEPTION(ModelBonesExceedsMaxBones);

// TODO: Destroy function + destructor
// TODO: Fix animFrame
// TODO: Move implementation to cpp
// TODO: GetFrame,SetFrame
// TODO: GetAnimation, SetAnimation
// TODO: Interpolation between frames
// TODO: Free OpenGL objects
// TODO: ResourceManager support
// TODO: Emissive texture
// TODO: Entity Displacement map???
// TODO: Frame Buffer Object or Single upload call
// TODO: Events


class Model : public Entity, public Transformable
{
public:
	struct Animation
	{
		std::string name;
		float ticksPerSecond;
		float totalFrames;
	};
protected:
	static constexpr const int32_t maxBonesPerVertex = 8;
	static constexpr const int32_t maxBones = 100;

	struct Material
	{
		std::shared_ptr<Texture2D> diffuseTexture;
		bool blackKeyed = false;
	};

	std::vector<Material> materials;

	struct Mesh
	{
		GLuint VAO;
		std::vector<GLuint> buffers;
		uint32_t materialIndex;
		GLuint vertexCount;
	};

	std::vector<Mesh> meshes;

	std::vector<Animation> animations;

	struct NodeAnimation
	{
		std::map<float,glm::quat> rotationKeys;
		std::map<float,glm::vec3> positionKeys;
		std::map<float,glm::vec3> scaleKeys;
	};

	struct Node
	{
		int32_t boneIndex=-1;
		glm::mat4 transform {1.f};
		std::vector<Node> children;
		std::vector<NodeAnimation> anims;
	};

	Node rootNode;
	mutable std::shared_ptr<Shader> shader;

	bool playing = true;
	float animFrame=0;
	uint32_t currentAnim=0;

	std::vector<glm::mat4> boneMatrices;
	std::vector<glm::mat4> boneOffsets;

	std::map<std::string, uint32_t> boneMap;
public:
	void CreateFromFile(ResourceManager& resmg,const std::string filename);

	void Destroy()
	{
		boneMap.clear();
		boneMatrices.clear();
		boneOffsets.clear();
		animations.clear();
		meshes.clear();
	}

	void SetAnimation(const uint32_t& anim)
	{
		animFrame = 0;
		currentAnim = anim;
	}

	const uint32_t& GetAnimation() const noexcept
	{
		return currentAnim;
	}

	const uint32_t GetAnimationCount() const noexcept
	{
		return animations.size();
	}

	const std::vector<Animation>& GetAnimations() const
	{
		return animations;
	}

	virtual void Update()
	{
		UpdateModelMatrix();
		shader->Use();

		shader->SetUniform(0, model);
		shader->SetUniform(1, 0); // Texture

		if(!animations.size() || !playing) return;
		// Update animations

		animFrame = std::fmod(animFrame + info.deltaTime*animations[currentAnim].ticksPerSecond,animations[currentAnim].totalFrames);
		UpdateNode(rootNode,glm::mat4(1.f));
	}

	virtual void Draw() const
	{
		shader->Use();
		for(int32_t i=0;i<boneMatrices.size();i++)
		{
			shader->SetUniform(shader->GetUniformLocation(std::string("bones[" + std::to_string(i)+ "]").c_str()),boneMatrices[i]);
		}

		glActiveTexture(GL_TEXTURE0);
		
		for (auto& m : meshes)
		{
			glBindVertexArray(m.VAO);
			
			if(materials[m.materialIndex].diffuseTexture)
				materials[m.materialIndex].diffuseTexture->Bind();
			shader->SetUniform(2,materials[m.materialIndex].blackKeyed);
			shader->SetUniform(3,static_cast<bool>(materials[m.materialIndex].diffuseTexture)); // hasTexture

			if(materials[m.materialIndex].blackKeyed) glDisable(GL_CULL_FACE); 
			glDrawElements(GL_TRIANGLES, m.vertexCount, GL_UNSIGNED_INT, 0);
			if(materials[m.materialIndex].blackKeyed) glEnable(GL_CULL_FACE);  
		}
		glBindVertexArray(0);
	}

	const bool& IsPlaying() const noexcept
	{
		return playing;
	}

	void Play() noexcept
	{
		playing = true;
	}

	void Pause() noexcept
	{
		playing = false;
	}

	void Stop() noexcept
	{
		playing = false;
		animFrame = 0;
	}

	void Toggle() noexcept
	{
		playing = !playing;
	}

protected:
	void LoadMaterials(const aiScene* scene,ResourceManager& resmg,const std::filesystem::path& directory)
	{
		materials.resize(scene->mNumMaterials);
		for(uint32_t m = 0 ; m < scene->mNumMaterials; ++m)
		{
			aiString source;


			if(scene->mMaterials[m]->GetTextureCount(aiTextureType_DIFFUSE) > 0)
			{
				if(scene->mMaterials[m]->GetTexture(aiTextureType_DIFFUSE, 0, &source) == aiReturn_SUCCESS)
				{
					materials[m].blackKeyed = std::string(source.C_Str()) == "textures\\SH3.png";
					rethrow(materials[m].diffuseTexture = LoadTexture(scene,resmg,m,source.C_Str(),directory));
				}
			}
		}
	}

	std::shared_ptr<Texture2D> LoadTexture(const aiScene* scene,ResourceManager& resmg,const uint32_t& matId,const char* source,const std::filesystem::path& directory)
	{
		std::cout << "Material: "<< source << std::endl;
		if(source[0] == '*') // Embedded texture
		{
			const uint32_t texId = atoi(source+1);
			auto texture = scene->mTextures[texId];
			if(texture->mHeight == 0)
			{
				return resmg.LoadResource<Texture2DResource>({texture->pcData,texture->mWidth,false});
			} else {
				return resmg.LoadResource<Texture2DResource>({texture->pcData,texture->mWidth*texture->mHeight*4,{texture->mWidth,texture->mHeight},GL_RGB,GL_RGBA,GL_UNSIGNED_BYTE});
			}
		}
		std::filesystem::path path(directory);
		std::string texturePath(source);
		std::replace(texturePath.begin(),texturePath.end(),'\\','/');
		path += texturePath;
	
		// Guess extension if missing
		if(!path.has_extension())
		{
			bool foundExtension = false;
			for(auto& e : {"png","jpg","jpeg"})
			{
				path.replace_extension(e);
				if(std::ifstream(path)) // If file exists
				{
					foundExtension = true;
					break;
				}
			}
			if(!foundExtension)
			{
				path.replace_extension();
				throw Exception(ExceptionType::ModelNoExtensionFoundForTexture,"Extension cannot be found for texture: \v",path);
			}
		}

		return resmg.LoadResource<Texture2DResource>({path.c_str(),TextureSourceType::CompressedFile});
	}

	void LoadNode(Node& node, const aiNode* sourceNode,const aiScene* scene)
	{
		node.transform = mat4_cast(sourceNode->mTransformation);

		if(boneMap.find(sourceNode->mName.C_Str()) != boneMap.end())
			node.boneIndex = boneMap[sourceNode->mName.C_Str()];
		else
			node.boneIndex = -1;
		
		// Load keyframes
		node.anims.resize(scene->mNumAnimations);
		for(uint32_t i =0; i < scene->mNumAnimations;i++)
		{
			for(uint32_t j =0; j < scene->mAnimations[i]->mNumChannels;j++)
			{
				if(scene->mAnimations[i]->mChannels[j]->mNodeName != sourceNode->mName) continue;
				const aiNodeAnim* anim = scene->mAnimations[i]->mChannels[j];

				for(uint32_t k =0;k<anim->mNumPositionKeys;k++)
				{
					node.anims[i].positionKeys.insert(std::make_pair(static_cast<float>(anim->mPositionKeys[k].mTime),vec3_cast(anim->mPositionKeys[k].mValue)));
				}

				for(uint32_t k =0;k<anim->mNumRotationKeys;k++)
				{
					node.anims[i].rotationKeys.insert(std::make_pair(static_cast<float>(anim->mRotationKeys[k].mTime),quat_cast(anim->mRotationKeys[k].mValue)));
				}

				for(uint32_t k =0;k<anim->mNumScalingKeys;k++)
				{
					node.anims[i].scaleKeys.insert(std::make_pair(static_cast<float>(anim->mScalingKeys[k].mTime),vec3_cast(anim->mScalingKeys[k].mValue)));
				}
			}
		}

		for (uint32_t i = 0; i < sourceNode->mNumChildren; i++)
		{
			node.children.push_back({});
			LoadNode(node.children.back(),sourceNode->mChildren[i],scene);
		}
	}

	void LoadAnimations(const aiScene* scene)
	{
		if(!scene->HasAnimations()) return;

		animations.resize(scene->mNumAnimations);
		for(uint32_t i =0;i<scene->mNumAnimations;i++)
		{
			const std::string name(scene->mAnimations[i]->mName.C_Str());
			animations[i].name = name.substr(name.find_last_of('|')+1);
			animations[i].ticksPerSecond = scene->mAnimations[i]->mTicksPerSecond;
			animations[i].totalFrames = scene->mAnimations[i]->mDuration;
		}
	}

	void UpdateNode(const Node& node, const glm::mat4& parentTransform)
	{
		glm::mat4 nodeTransform (1.f);
		bool hasAnim = false;
		
		switch(node.anims[currentAnim].positionKeys.size())
		{
			case 0: break;
			case 1:
			{
				hasAnim = true;
				nodeTransform = glm::translate(nodeTransform,node.anims[currentAnim].positionKeys.begin()->second);
				break;
			}
			default:
			{
				hasAnim = true;
				auto nextKey = node.anims[currentAnim].positionKeys.upper_bound(animFrame);
				const auto currentKey = std::prev(nextKey);

				if(nextKey == node.anims[currentAnim].positionKeys.end())
				{
					nextKey = node.anims[currentAnim].positionKeys.begin();
				}
				float factor = (animFrame - currentKey->first)/std::abs(nextKey->first - currentKey->first);
				if(!std::isfinite(factor)) factor = 0;
				
				nodeTransform = glm::translate(
					nodeTransform,
					glm::mix(currentKey->second,nextKey->second,factor)
				);
			}
		}
		
		switch(node.anims[currentAnim].rotationKeys.size())
		{
			case 0: break;
			case 1:
			{
				hasAnim = true;
				nodeTransform *= glm::mat4_cast(node.anims[currentAnim].rotationKeys.begin()->second);
				break;
			}
			default:
			{
				hasAnim = true;
				auto nextKey = node.anims[currentAnim].rotationKeys.upper_bound(animFrame);
				const auto currentKey = std::prev(nextKey);

				if(nextKey == node.anims[currentAnim].rotationKeys.end())
				{
					nextKey = node.anims[currentAnim].rotationKeys.begin();
				}

				float factor = (animFrame - currentKey->first)/std::abs(nextKey->first - currentKey->first);
				if(!std::isfinite(factor)) factor = 0;
				nodeTransform *= glm::mat4_cast(
					glm::slerp(currentKey->second,nextKey->second,factor)
				);
			}
		}
		
		switch(node.anims[currentAnim].scaleKeys.size())
		{
			case 0: break;
			case 1:
			{
				hasAnim = true;
				nodeTransform = glm::scale(nodeTransform,node.anims[currentAnim].scaleKeys.begin()->second);
				break;
			}
			default:
			{
				hasAnim = true;
				auto nextKey = node.anims[currentAnim].scaleKeys.upper_bound(animFrame);
				const auto currentKey = std::prev(nextKey);

				if(nextKey == node.anims[currentAnim].scaleKeys.end())
				{
					nextKey = node.anims[currentAnim].scaleKeys.begin();
				}
				float factor = (animFrame - currentKey->first)/std::abs(nextKey->first - currentKey->first);
				if(!std::isfinite(factor)) factor = 0;
				nodeTransform = glm::scale(
					nodeTransform,
					glm::mix(currentKey->second,nextKey->second,factor)
				);
			}
		}

		if(!hasAnim) nodeTransform = node.transform;

		const glm::mat4 globalTransform = parentTransform * nodeTransform;

		if(node.boneIndex >= 0)
		{
			boneMatrices[node.boneIndex] =  globalTransform * boneOffsets[node.boneIndex];
		}
		
		// Recurse through children
		for(const auto& c : node.children) UpdateNode(c,globalTransform);

	}

	void LoadMeshes(const aiScene* scene)
	{
		uint32_t boneCount = 0;

		meshes.resize(scene->mNumMeshes);
		for(uint32_t i = 0; i < scene->mNumMeshes;i++)
		{
			glGenVertexArrays(1, &meshes[i].VAO);
			glBindVertexArray(meshes[i].VAO);

			meshes[i].vertexCount = scene->mMeshes[i]->mNumFaces * 3;
			meshes[i].materialIndex = scene->mMeshes[i]->mMaterialIndex;

			if (scene->mMeshes[i]->HasPositions())
			{
				meshes[i].buffers.push_back(0);
				glGenBuffers(1, &meshes[i].buffers.back());
				glBindBuffer(GL_ARRAY_BUFFER, meshes[i].buffers.back());

				glBufferData(GL_ARRAY_BUFFER, scene->mMeshes[i]->mNumVertices * sizeof(aiVector3D), scene->mMeshes[i]->mVertices, GL_STATIC_DRAW);

				glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(aiVector3D), reinterpret_cast<void*>(0));
				glEnableVertexAttribArray(0);
			}
			
			if (scene->mMeshes[i]->HasNormals())
			{
				meshes[i].buffers.push_back(0);
				glGenBuffers(1, &meshes[i].buffers.back());
				glBindBuffer(GL_ARRAY_BUFFER, meshes[i].buffers.back());

				glBufferData(GL_ARRAY_BUFFER, scene->mMeshes[i]->mNumVertices * sizeof(aiVector3D), scene->mMeshes[i]->mNormals, GL_STATIC_DRAW);

				glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(aiVector3D), reinterpret_cast<void*>(0));
				glEnableVertexAttribArray(1);
			}

			if (scene->mMeshes[i]->HasTextureCoords(0))
			{
				meshes[i].buffers.push_back(0);
				glGenBuffers(1, &meshes[i].buffers.back());
				glBindBuffer(GL_ARRAY_BUFFER, meshes[i].buffers.back());

				glBufferData(GL_ARRAY_BUFFER, scene->mMeshes[i]->mNumVertices * sizeof(aiVector3D), scene->mMeshes[i]->mTextureCoords[0], GL_STATIC_DRAW);

				glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE,sizeof(aiVector3D) , reinterpret_cast<void*>(0));
				glEnableVertexAttribArray(2);
			}

			if (scene->mMeshes[i]->HasFaces())
			{
				std::vector<GLuint> indices(scene->mMeshes[i]->mNumFaces * 3U);
				for (uint32_t j = 0; j < scene->mMeshes[i]->mNumFaces; j++) {
					indices[j * 3] = scene->mMeshes[i]->mFaces[j].mIndices[0];
					indices[j * 3 + 1] = scene->mMeshes[i]->mFaces[j].mIndices[1];
					indices[j * 3 + 2] = scene->mMeshes[i]->mFaces[j].mIndices[2];
				}
				meshes[i].buffers.push_back(0);

				glGenBuffers(1, &meshes[i].buffers.back());
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, meshes[i].buffers.back());
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, scene->mMeshes[i]->mNumFaces * sizeof(GLuint) * 3U, indices.data(), GL_STATIC_DRAW);
			}

			if (scene->mMeshes[i]->HasBones())
			{
				std::vector<std::array<int32_t,maxBonesPerVertex>> vertexBoneId(scene->mMeshes[i]->mNumVertices,{-1,-1,-1,-1,-1,-1,-1,-1});
				std::vector<std::array<float,maxBonesPerVertex>> vertexBoneWeight(scene->mMeshes[i]->mNumVertices,{0});
				for(uint32_t j = 0; j < scene->mMeshes[i]->mNumBones;j++)
				{
					uint32_t boneIndex = 0;
					const char* boneName = scene->mMeshes[i]->mBones[j]->mName.data;
					if(boneMap.find(boneName) == boneMap.end())
					{
						boneIndex = boneCount++;

						if(boneIndex >= maxBones)
							throw Exception(ExceptionType::ModelBonesExceedsMaxBones, "Model bones are exceeding maxBones(\v)",maxBones);

						boneOffsets.push_back(mat4_cast(scene->mMeshes[i]->mBones[j]->mOffsetMatrix));
						boneMap[boneName] = boneIndex;
					}
					else
					{
						boneIndex = boneMap[boneName];
					}

					for(uint32_t k = 0; k < scene->mMeshes[i]->mBones[j]->mNumWeights;k++)
					{
						const aiVertexWeight& weight = scene->mMeshes[i]->mBones[j]->mWeights[k];
						bool found = false;
						int32_t lowest = 0;
						for(uint32_t l = 0;l< maxBonesPerVertex;l++)
						{
							if(vertexBoneId[weight.mVertexId][l] == -1)
							{
								found = true;
								vertexBoneWeight[weight.mVertexId][l] = weight.mWeight;
								vertexBoneId[weight.mVertexId][l] = boneIndex;
								break;
							}

							if(vertexBoneWeight[weight.mVertexId][l] < vertexBoneWeight[weight.mVertexId][lowest])
							{
								lowest = l;
							}
						}
						if(!found && vertexBoneWeight[weight.mVertexId][lowest] < weight.mWeight)
						{
							//Warning:
							//std::cout << "No place for bone!" << std::endl;
							vertexBoneWeight[weight.mVertexId][lowest] = weight.mWeight;
							vertexBoneId[weight.mVertexId][lowest] = boneIndex;
						}
					}
				}

				{
					meshes[i].buffers.push_back(0);
					glGenBuffers(1, &meshes[i].buffers.back());
					glBindBuffer(GL_ARRAY_BUFFER, meshes[i].buffers.back());

					glBufferData(GL_ARRAY_BUFFER, scene->mMeshes[i]->mNumVertices * sizeof(int32_t) * maxBonesPerVertex,vertexBoneId.data(), GL_STATIC_DRAW);
					
					glVertexAttribIPointer(3, 4, GL_INT,sizeof(int32_t)*maxBonesPerVertex, reinterpret_cast<void*>(0));
					glEnableVertexAttribArray(3);
				}

				{
					meshes[i].buffers.push_back(0);
					glGenBuffers(1, &meshes[i].buffers.back());
					glBindBuffer(GL_ARRAY_BUFFER, meshes[i].buffers.back());

					glBufferData(GL_ARRAY_BUFFER, scene->mMeshes[i]->mNumVertices * sizeof(float)*maxBonesPerVertex,vertexBoneWeight.data(), GL_STATIC_DRAW);
					
					glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE,sizeof(float) * maxBonesPerVertex, reinterpret_cast<void*>(0));
					glEnableVertexAttribArray(4);
				}
			}
		}
	}
};
