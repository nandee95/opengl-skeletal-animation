#pragma once
#include <stdint.h>
#include "Camera.hpp"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

class Application
{
	static constexpr float const cameraSensitivity = 0.01;
	static constexpr float const cameraSpeed = 1000;
	static constexpr float const cameraTurnRate = 3.1415 / 3;
	static constexpr float modelSpeed = 1000.f;
	Camera camera;

	bool wireFrame = false;
	bool attachedCamera = false;

public:
	int32_t Run();
	static void CursorPosCallback(GLFWwindow *window, double x, double y);
	static void KeyCallback(GLFWwindow *window, int key, int scancode, int action, int mods);
	static void APIENTRY OpenGlDebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam);
};