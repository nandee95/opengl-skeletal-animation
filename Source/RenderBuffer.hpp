#pragma once

#include "FrameBuffer.hpp"

REGISTER_EXCEPTION(OpenGlRenderBufferCreationFailed)

class RenderBuffer : public FrameBuffer
{
	GLuint RBO;	
public:
	RenderBuffer(Texture2D& texture) : FrameBuffer(texture)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, this->FBO);
		glGenRenderbuffers(1, &RBO);
		glBindRenderbuffer(GL_RENDERBUFFER, RBO);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, texture.GetResolution().x,texture.GetResolution().y);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, RBO);
		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			throw Exception(ExceptionType::OpenGlRenderBufferCreationFailed,"Failed to create render buffer");
		Unbind();
	}
};
