#pragma once

#include <functional> 
#include <fstream>
#include <cstring>

typedef size_t ResourceHash;

class ResourceInfo
{
public:
	virtual ResourceHash CalcHash() const = 0;

protected:
	static size_t CombineHash(const size_t& first,const size_t& second) noexcept
	{
		return first ^ (second << 1);
	}
};

class Resource
{
public:
	static constexpr const ResourceHash DefaultHash = -1;
private:
	ResourceHash hash=DefaultHash;
	bool loaded=false,killme=false; // Flags
protected:
	void KillMe()
	{
		killme = true;
	}
public:
	virtual void Load(const void* info) = 0;

	size_t GetHash() const noexcept
	{
		return hash;
	}

protected:
	bool ReadStringFromFile(const std::string& filename,std::string& buffer) noexcept
	{
		// Open file
        std::ifstream ifs(filename,std::ios::ate);
        if(!ifs)	return false;

		// Get file size
        size_t size = ifs.tellg();
        ifs.seekg(std::ios::beg);
        size -= ifs.tellg();

		if(size == 0) return false;

		// Read file
		buffer.resize(size);
        ifs.read(buffer.data(),size);
		return true;
	}
};