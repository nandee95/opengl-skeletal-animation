#pragma once
#include <stdint.h>
#include <tuple>
#include <type_traits>

// Meta programming related code
// Compile time only!

#define FIRST_ARG(ARG1,...) ARG1
#define SECOND_ARG(ARG1,ARG2,...) ARG2

#define EXCEPT_FIRST_ARG(ARG1,...) __VA_ARGS__
#define EXCEPT_FIRST_TWO_ARG(ARG1,ARG2,...) __VA_ARGS__

#define ARG_COUNT(...) std::tuple_size<decltype(std::make_tuple(__VA_ARGS__))>::value
#define ARGS_MATCH(format,...) Meta::CountCharacters(format,Meta::VarKey) == ARG_COUNT(__VA_ARGS__)

namespace Meta
{
    constexpr const char VarKey = '\v';

    constexpr int32_t CountCharacters(const char* str,const char ch)
	{
		int32_t count = 0;
		for (int32_t i = 0; str[i] != '\0'; i++)
		{
			if (str[i] == ch) count++;
		}
		return count;
	}

	template<typename T>
	constexpr bool IsString()
	{
		return std::is_same<T,char*>::value || std::is_same<T,std::string>::value;
	};

};