#pragma once
#include "Exception.hpp"
#include "Resource.hpp"

#include <memory>
#include <iostream> // TODO
#include <map>

class ResourceManager
{
	std::map<size_t, std::shared_ptr<Resource>> resources; 
public:
	template <typename T,typename I>
	std::shared_ptr<T> LoadResource(const I& info)
	{
		// Compile time check
		static_assert(std::is_base_of<Resource,T>::value,"Type is not derived from \"Resource\" class");
		// Generate hash
		const ResourceHash hash = reinterpret_cast<const ResourceInfo*>(&info)->CalcHash();

		// If resource is already stored
		{
			auto storedResource = resources.find(hash);
			if(storedResource != resources.end())
			{
				std::cout << "Old resource loaded! Hash: "<< hash << std::endl;
				return std::dynamic_pointer_cast<T>(storedResource->second); // TODO
			}
		}

		std::cout << "New resource created! Hash: "<< hash << std::endl;
		auto createdResource = std::make_shared<T>();
		rethrow(createdResource->Load(&info));
		resources.insert(std::make_pair(hash,createdResource));
		return createdResource;
	}

	void Clean()
	{
		for(auto it = resources.begin();it!=resources.end();)
		{
			if(it->second.use_count() <= 1)
				it = resources.erase(it);
			else 
				it++;
		}
	}

};