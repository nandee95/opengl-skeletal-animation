#pragma once

class FullscreenQuad
{
protected:
	GLuint vao;
public:
	FullscreenQuad()
	{
		glGenVertexArrays(1,&vao);
	}

	~FullscreenQuad()
	{
		glDeleteVertexArrays(1,&vao);
	}

	void Draw()
	{
		glBindVertexArray(vao);
	   	glDrawArrays(GL_TRIANGLE_STRIP,0,4);
	}
};