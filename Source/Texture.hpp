#pragma once
#include "Exception.hpp"
#include "Resource.hpp"

#include <GL/glew.h>
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>

#define Texture2DResource Texture2D,TextureInfo

REGISTER_EXCEPTION(TextureFailedToOpenFile);
REGISTER_EXCEPTION(TextureInvalidSourceType);

enum class TextureSourceType : uint8_t
{
	_Unknown,
	CompressedFile,
	CompressedMemory,
	RawMemory,
	RawFile,
	_Max
};

class Texture2D;

class TextureInfo : public ResourceInfo
{
protected:
	friend Texture2D;
	TextureSourceType type;		// Texture source type
	const char* source;			// Filename or byte array
	size_t size = 0;			// Byte array size
	GLenum texType,texInternalFormat,texFormat;	// Data type
	glm::uvec2 resolution{0,0};	// Size
	bool alpha = false;
public:

	TextureInfo(const void* data,const size_t& size,const glm::uvec2& resolution,const GLenum& texInternalFormat=GL_RGB, const GLenum& texFormat=GL_RGB,const GLenum& texType=GL_UNSIGNED_BYTE)
	 : source(reinterpret_cast<const char*>(data)),
	 resolution(resolution),
	 texInternalFormat(texInternalFormat),
	 texFormat(texFormat),
	 texType(texType),
	 size(size),
	 type(TextureSourceType::RawMemory)
	{	}

	// Constructor for CompressedFile and RawFile
	TextureInfo(const char* filename, const TextureSourceType& type = TextureSourceType::CompressedFile)
		: source(filename),
		type(type)
	{	}
	
	// 
	TextureInfo(const void* source,const size_t& size,const bool& alpha=false)
		: source(reinterpret_cast<const char*>(source)),
		size(size),
		alpha(alpha),
		type(TextureSourceType::CompressedMemory)
	{	}

	virtual ResourceHash CalcHash() const
	{
		switch(type)
		{
			case TextureSourceType::CompressedFile:
			case TextureSourceType::RawFile:
				return std::hash<std::string>()(source);
			case TextureSourceType::CompressedMemory:
				return std::hash<std::string>()(std::string(source,size));
			case TextureSourceType::RawMemory:
				std::hash<int32_t> intHash;
				size_t result = intHash(resolution.x);
				result = CombineHash(result,intHash(resolution.y));
				result = CombineHash(result,intHash(texInternalFormat));
				result = CombineHash(result,intHash(texFormat));
				result = CombineHash(result,intHash(texType));
				return CombineHash(result, std::hash<std::string>()(std::string(source,size)));
		}
		throw Exception(ExceptionType::TextureInvalidSourceType,"Invalid texture source type");
		return -1;
	}
};

class Texture2D : public Resource
{
	GLuint texture=0;
	glm::uvec2 resolution;
public:
	virtual void Load(const void* info);

	void Create(const glm::uvec2& resolution,const void* data,const GLenum& internalFormat=GL_RGB, const GLenum& format=GL_RGB,const GLenum& type=GL_UNSIGNED_BYTE);
	void CreateFromFile(const char* filename);
	void CreateFromMemory(const void* compressedData,const size_t& size,const bool& alpha);
	void Destroy() noexcept;

	inline const glm::uvec2& GetResolution() const noexcept
	{
		return resolution;
	}

	inline bool IsValid() const noexcept
	{
		return texture != 0;
	}

	inline void Bind() const noexcept
	{
		glBindTexture(GL_TEXTURE_2D, texture);
	}

	inline void Attach(const GLenum& slot = GL_TEXTURE0)
	{
		glActiveTexture(slot);
		glBindTexture(GL_TEXTURE_2D, texture);
	}

	/*
	Sets texture wrapping for the texture.
	Possible values:
	GL_REPEAT,GL_MIRRORED_REPEAT,GL_CLAMP_TO_EDGE,GL_CLAMP_TO_BORDER
	*/
	inline void SetWrap(const GLenum& wrapS = GL_REPEAT, const GLenum& wrapT=GL_REPEAT)
	{
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapS);	
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapS);
	}

	/*
	Sets texture wrapping to a single color for the texture.
	*/
	inline void SetWrapColor(const glm::vec4& color)
	{
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);	
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, reinterpret_cast<const GLfloat*>(&color)); 
	}

	/*
	Sets filtering for the texture.
	Possible values:
	GL_NEAREST, GL_LINEAR, GL_LINEAR_MIPMAP_LINEAR(only minFilter)
	*/
	inline void SetFilter(const GLenum& minFilter = GL_NEAREST, const GLenum& magFilter=GL_REPEAT)
	{
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilter);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magFilter);
	}

	/*
	Gets texture OpenGL id.
	*/
	inline GLuint GetId() const noexcept
	{
		return texture;
	}
};