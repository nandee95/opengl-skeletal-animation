#pragma once
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <assimp/matrix4x4.h>
#include <assimp/vector3.h>
#include <assimp/quaternion.h>

inline glm::mat4 mat4_cast(const aiMatrix4x4& from)
{
	return glm::transpose(glm::make_mat4(&from.a1));
}

inline glm::vec3 vec3_cast(const aiVector3D& from)
{
	return {from.x,from.y,from.z};
}

inline glm::quat quat_cast(const aiQuaternion& from)
{
	return {from.w,from.x,from.y,from.z};
}
