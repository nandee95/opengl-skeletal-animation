#include "Application.hpp"
#include <iostream>
#include <memory>
#include <vector>
#include <chrono>
#include <thread>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "Camera.hpp"
#include "Grid.hpp"
#include "Cube.hpp"
#include "UniformBuffer.hpp"
#include "Model.hpp"
#include "ResourceManager.hpp"
#include "RenderBuffer.hpp"
#include "FullscreenQuad.hpp"

#include "Text.hpp"

std::shared_ptr<Model> gModel;
std::shared_ptr<Text> gText;

int32_t Application::Run()
{
	// Set window hints
	glfwInit();
	glfwWindowHint(GLFW_VERSION_MAJOR,4);
	glfwWindowHint(GLFW_VERSION_MINOR,5);
	glfwWindowHint(GLFW_RESIZABLE,GLFW_FALSE);
	glfwWindowHint(GLFW_DOUBLEBUFFER,GL_TRUE);
	glfwWindowHint(GLFW_SAMPLES, 1);
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);

	// Create window
	GLFWwindow* window;
	const glm::vec2 windowSize{1280,720};
	if(!(window = glfwCreateWindow(windowSize.x,windowSize.y,"OpenGl Skeletal animation",nullptr,nullptr)))
	{
		std::cerr << "Failed to create window" << std::endl;
		return EXIT_FAILURE;
	}

	// Window setup
	glfwSetCursorPosCallback(window,CursorPosCallback);
	glfwSetKeyCallback(window,KeyCallback);
	glfwSetWindowUserPointer(window,this);
	glfwMakeContextCurrent(window);

	// Initialize OpenGL
	glewExperimental = GL_TRUE; 
	glewInit();
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_MULTISAMPLE);
	glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);  
	glClearColor(0.03,0.03,0.03,1);

	// Enable debug context
	{
		GLint flags;
		glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
		if (flags & GL_CONTEXT_FLAG_DEBUG_BIT)
		{
			glEnable(GL_DEBUG_OUTPUT);
			glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS); 
			glDebugMessageCallback(OpenGlDebugCallback, this);
			glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
			std::cout << "Debug context is active!" << std::endl;
		}
		else
		{
			std::cout << "Debug context is unavailable!" << std::endl;
		}
	}

	// Initialize camera
	camera.SetAspectRatio(windowSize.x/windowSize.y);
	camera.SetPositon({1000,1000,1000});
	Entity::info.proj = camera.CalcProjMatrix();

	// Projection & view matrix uniform buffer
	UniformBuffer projView;
	projView.Create(nullptr,sizeof(GLfloat)*4*4,GL_STREAM_DRAW);
	projView.BindBufferBase(0);

	// Create resource manager
	ResourceManager resmg;

	// Create entities
	std::vector<std::shared_ptr<Entity>> entities;

	auto grid  = std::make_shared<Grid>();
	iffails(grid->Create(resmg,{-100,100},{-100,100},0,1000.f),e)
	{
		std::cerr << "Failed to create grid!" << std::endl;
		std::cerr << e.GetMessage() << std::endl;
		return EXIT_FAILURE;
	}
	grid->SetColor({0,1,1,1});
	entities.push_back(grid);

	auto cube  = std::make_shared<Cube>();
	iffails(cube->Create(),e)
	{
		std::cerr << "Failed to create cube!" << std::endl;
		std::cerr << e.GetMessage() << std::endl;
		return EXIT_FAILURE;
	}
	cube->SetScale({1000,1000,1000});
	cube->SetPosition({2000,2000,0});
	entities.push_back(cube);

	
	auto model  = std::make_shared<Model>();
	iffails(model->CreateFromFile(resmg,"../Resources/Spider/Spider.fbx"),e)
	{
		std::cerr << "Failed to create model!" << std::endl;
		std::cerr << e.GetMessage() << std::endl;
		return EXIT_FAILURE;
	}
	gModel = model;
	model->SetScale({10,10,10});

	model->SetOrientation({glm::half_pi<float>(),0,0});
	entities.push_back(model);
	
	auto start = std::chrono::high_resolution_clock::now();

	auto text  = std::make_shared<Text>();
	iffails(text->Create(resmg,"Text demo"),e)
	{
		std::cerr << "Failed to create text!" << std::endl;
		std::cerr << e.GetMessage() << std::endl;
		return EXIT_FAILURE;
	}
	text->SetOrientation({glm::half_pi<float>(),0,0});
	text->SetPosition({2500,2500,2000});
	text->SetColor({0.1,1,0.3,1});
	text->SetOrigin({text->GetWidth() / 2,0,0});
	entities.push_back(text);
	gText = text;

	std::chrono::high_resolution_clock::time_point lastFrame = std::chrono::high_resolution_clock::now();

	Texture2D outputTexture;
	outputTexture.Create({1280,720},nullptr,GL_RGB,GL_RGB,GL_UNSIGNED_BYTE);
	RenderBuffer fbo(outputTexture);
	FullscreenQuad fsQuad;

	Shader postProcessorShader;
	iffails(postProcessorShader.CreateFromSourceFile("../Shaders/FullscreenQuad.vert","../Shaders/PostProcessing.frag"),e)
	{
		std::cout << "Failed to compile blur shader: \v" << e.GetMessage() << std::endl;
		return EXIT_FAILURE;
	}


	uint32_t oldAnimation=0;
	while(!glfwWindowShouldClose(window))
	{
		// Update frame info
		std::chrono::high_resolution_clock::time_point currentFrame = std::chrono::high_resolution_clock::now();
		Entity::info.deltaTime = std::chrono::duration_cast<std::chrono::milliseconds>(currentFrame-lastFrame).count()/1000.f;
		Entity::info.view = camera.CalcViewMatrix();
		Entity::info.elapsedTime =  std::chrono::duration_cast<std::chrono::milliseconds>(currentFrame - start).count() / 1e3;
		
		// Update uniform buffers
		glm::mat4 baseMat = Entity::info.proj * Entity::info.view;
		projView.Upload(glm::value_ptr(baseMat),0,sizeof(baseMat));

		// Update controls
		if(!attachedCamera)
		{
			camera.Move({
				(glfwGetKey(window,GLFW_KEY_W) ? 1.f : (glfwGetKey(window,GLFW_KEY_S) ? -1.f : 0.f)) * cameraSpeed * Entity::info.deltaTime,
				(glfwGetKey(window,GLFW_KEY_A) ? 1.f : (glfwGetKey(window,GLFW_KEY_D) ? -1.f : 0.f)) * cameraSpeed * Entity::info.deltaTime,
				(glfwGetKey(window,GLFW_KEY_SPACE) ? 1.f : (glfwGetKey(window,GLFW_KEY_LEFT_SHIFT) ? -1.f : 0.f)) * cameraSpeed * Entity::info.deltaTime,
			});
			camera.Turn({0,0,
				(glfwGetKey(window,GLFW_KEY_Q) ? 1.f : (glfwGetKey(window,GLFW_KEY_E) ? -1.f : 0.f)) * cameraTurnRate * Entity::info.deltaTime,
			});
		}

		// Update model
		
		uint32_t newAnimation = 14;
		bool moves = false;
		float movementDirection = 0.f;
		float strafe = 0.f;

		if(glfwGetKey(window,GLFW_KEY_RIGHT))
		{	
			moves = true;
			movementDirection = -glm::half_pi<float>();
			strafe = 1.f;
			newAnimation = 9;
		}
		else if(glfwGetKey(window,GLFW_KEY_LEFT))
		{
			moves = true;
			movementDirection = glm::half_pi<float>();
			strafe = -1.f;
			newAnimation = 8;
		}
		else if(glfwGetKey(window,GLFW_KEY_UP))
		{	
			moves = true;
			movementDirection = glm::pi<float>();
			newAnimation = 7;
		}
		else if(glfwGetKey(window,GLFW_KEY_DOWN))
		{
			moves = true;
			newAnimation = 6;
		}

		if(moves)
		{
			float roll = glm::roll(model->GetOrientation());
			movementDirection -=roll;
			model->SetPosition(model->GetPosition()-glm::vec3(sin(movementDirection),cos(movementDirection),0) * modelSpeed * Entity::info.deltaTime );
			model->SetOrientation(glm::rotate(model->GetOrientation(),strafe*-glm::quarter_pi<float>()*Entity::info.deltaTime,glm::vec3(0,1,0)));	
		}

		if(newAnimation != oldAnimation)
		{
			model->SetAnimation(newAnimation);
		}

		oldAnimation = newAnimation;

		if(attachedCamera)
		{
			float cameraDirection = -glm::eulerAngles(model->GetOrientation()).z + glm::half_pi<float>();
			camera.SetPositon(model->GetPosition() + glm::vec3(2000*std::cos(cameraDirection),-2000*std::sin(cameraDirection),1000));

			float roll = glm::roll(model->GetOrientation());
			camera.SetOrientation(glm::vec3(0,0,-roll - glm::half_pi<float>()));
		}

		// Update entities
		for(const auto& e : entities)
		{
			e->Update();
		}

		// Bind framebuffer
		fbo.Bind();

		// Clear frame
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		auto textOrientation = camera.GetPosition() - (text->GetPosition() - text->GetOrigin());
		//text->SetOrientation({glm::half_pi<float>(),0,Entity::info.elapsedTime * 3.f});
		text->SetOrientation({glm::half_pi<float>(),0,glm::half_pi<float>() + std::atan2(textOrientation.y,textOrientation.x)});
		// Draw entities
		glPolygonMode( GL_FRONT_AND_BACK, wireFrame ? GL_LINE : GL_FILL);
		for(const auto& e : entities)
		{
			e->Draw();
		}

		fbo.Unbind();

		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glActiveTexture(GL_TEXTURE20);
		outputTexture.Bind();
		postProcessorShader.Use();
		postProcessorShader.SetUniform(0,20);
		fsQuad.Draw();
		// Remove unused resources
		resmg.Clean();

		// Finish frame
		glfwSwapBuffers(window);
		glfwPollEvents();

		lastFrame = currentFrame;

		std::this_thread::sleep_for(std::chrono::microseconds(1000000/60));
	}

	glfwTerminate();
	return EXIT_SUCCESS;
}

void Application::CursorPosCallback(GLFWwindow* window, double x, double y)
{
	Application* app = reinterpret_cast<Application*>(glfwGetWindowUserPointer(window));
	if(app->attachedCamera) return;

	glm::ivec2 windowSize;
	glfwGetWindowSize(window,&windowSize.x,&windowSize.y);
	app->camera.Turn(glm::vec3(((static_cast<glm::vec2>(windowSize)/2.f)-glm::vec2{x,y}),0)*glm::vec3{-1,1,0} * cameraSensitivity);
	glfwSetCursorPos(window,windowSize.x/2,windowSize.y/2);
}

void Application::KeyCallback(GLFWwindow* window,int key,int scancode, int action, int mods)
{
	Application* app = reinterpret_cast<Application*>(glfwGetWindowUserPointer(window));
	switch(key)
	{
		case GLFW_KEY_ESCAPE:
		{
			glfwSetWindowShouldClose(window,GLFW_TRUE);
		}
		case GLFW_KEY_1:
		{
			if(action == GLFW_PRESS) app->wireFrame = !app->wireFrame;
			break;
		}
		case GLFW_KEY_2:
		{
			if(action == GLFW_PRESS)
			{
				gModel->SetAnimation((gModel->GetAnimation() + 1) % gModel->GetAnimationCount());
				std::cout << gModel->GetAnimation()<< " : " << gModel->GetAnimations()[gModel->GetAnimation()].name << std::endl;
			}
			break;
		}
		case GLFW_KEY_3:
		{
			if(action == GLFW_PRESS)
			{
				gModel->Toggle();
			}
			break;
		}
		case GLFW_KEY_4:
		{
			if(action == GLFW_PRESS)
			{
				app->attachedCamera = !app->attachedCamera;
			}
			break;
		}
	}
}

void APIENTRY Application::OpenGlDebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam)
{
    if(id == 131169 || id == 131185 || id == 131218 || id == 131204) return; 
	std::cerr << message << std::endl;
}