#pragma once
#include "Exception.hpp"
#include "Resource.hpp"

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#define ShaderResource Shader,ShaderInfo

REGISTER_EXCEPTION(ShaderInvalidSourceType);
REGISTER_EXCEPTION(ShaderFailedToOpenVertexSourceFile);
REGISTER_EXCEPTION(ShaderFailedToOpenGeometrySourceFile);
REGISTER_EXCEPTION(ShaderFailedToOpenFragmentSourceFile);
REGISTER_EXCEPTION(ShaderFailedToCreateProgram);
REGISTER_EXCEPTION(ShaderFailedToCreateShader);
REGISTER_EXCEPTION(ShaderFailedToCompileVertexShader);
REGISTER_EXCEPTION(ShaderFailedToCompileGeometryShader);
REGISTER_EXCEPTION(ShaderFailedToCompileFragmentShader);
REGISTER_EXCEPTION(ShaderFailedToLinkShaderProgram);

enum class ShaderSourceType : uint8_t
{
	_Unknown,
	SourceFile,
	SourceMemory,
	SpirvFile,
	SpirvMemory,
	_Max
};

struct ShaderInfo : public ResourceInfo
{
	std::string vertexSource;
	std::string fragmentSource;
	ShaderSourceType type= ShaderSourceType::_Unknown;
	ShaderInfo(const char* vertexSource,const char* fragmentSource,const ShaderSourceType& type): vertexSource(vertexSource),fragmentSource(fragmentSource),type(type)
	{	}

	virtual ResourceHash CalcHash() const
	{
		return CombineHash(std::hash<std::string>()(vertexSource),
			std::hash<std::string>()(fragmentSource)
		);
	}
};


//TODO: Implement all uniform functions
//TODO: Implement Spirv source

class Shader : public Resource
{
protected:
	GLuint program=0;
public:
	virtual void Load(const void* infoPtr)
	{
		ShaderInfo info = *reinterpret_cast<const ShaderInfo*>(infoPtr);

		switch(info.type)
		{
			case ShaderSourceType::SourceFile:
			{
				if(!ReadStringFromFile(info.vertexSource,info.vertexSource))
					throw Exception(ExceptionType::ShaderFailedToOpenVertexSourceFile,"Failed to open Vertex source file: \v",info.vertexSource);
				if(!ReadStringFromFile(info.fragmentSource,info.fragmentSource))
					throw Exception(ExceptionType::ShaderFailedToOpenFragmentSourceFile,"Failed to open Fragment source file: \v",info.fragmentSource);
			}
			case ShaderSourceType::SourceMemory:
			{
				rethrow(CreateFromSourceMemory(info.vertexSource.c_str(),info.fragmentSource.c_str()));
				break;
			}
			default:
			{
				throw Exception(ExceptionType::ShaderInvalidSourceType,"Unknown shader source type (\v)",static_cast<int32_t>(info.type));
			}
		}

	}

	~Shader() noexcept
	{
		Destroy();
	}

    void CreateFromSourceFile(const char* vertexSourceFile,const char* fragmentSourceFile);
	void CreateFromSourceMemory(const char* vertexSource,const char* fragmentSource);

	inline void Destroy() noexcept
	{
		if(program)
		{
			glDeleteProgram(program);
			program = 0;
		}
	}

	inline void Use() const noexcept
	{
		glUseProgram(program);
	}

	inline GLuint GetProgramId() const noexcept
	{
		return program;
	}

	inline const GLint GetAttribLocation(const char* attrib) const noexcept
	{
		return glGetAttribLocation(program, attrib);
	}

	inline const GLint GetUniformLocation(const char* uniform) const noexcept
	{
		return glGetUniformLocation(program, uniform);
	}

	inline void SetUniform(const GLuint& location, const GLint& value) noexcept
	{
		glUniform1i(location, value);
	}

	inline void SetUniform(const GLuint& location, const GLuint& value) noexcept
	{
		glUniform1ui(location, value);
	}


	inline void SetUniform(const GLuint& location, const GLfloat& value) noexcept
	{
		glUniform1f(location, value);
	}

	inline void SetUniform(const GLuint& location, const GLdouble& value) noexcept
	{
		glUniform1d(location, value);
	}

	inline void SetUniform(const GLuint& location, const glm::vec2& value) noexcept
	{
		glUniform2f(location, value.x, value.y);
	}

	inline void SetUniform(const GLuint& location, const glm::vec3& value) noexcept
	{
		glUniform3f(location, value.x, value.y, value.z);
	}

	inline void SetUniform(const GLuint& location, const glm::vec4& value) noexcept
	{
		glUniform4f(location, value.x, value.y, value.z, value.w);
	}

	inline void SetUniform(const GLuint& location, const glm::mat2& value) noexcept
	{
		glUniformMatrix2fv(location, 1, GL_FALSE, glm::value_ptr(value));
	}

	inline void SetUniform(const GLuint& location, const glm::mat3& value) noexcept
	{
		glUniformMatrix3fv(location, 1, GL_FALSE, glm::value_ptr(value));
	}

	inline void SetUniform(const GLuint& location, const glm::mat4& value) noexcept
	{
		glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(value));
	}

	inline void SetUniformArray(const GLuint& location, const std::vector<glm::mat4>& value) noexcept
	{
		glUniformMatrix4fv(location, value.size(), GL_FALSE, reinterpret_cast<const GLfloat*>(value.data()));
	}

};