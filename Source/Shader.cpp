#include "Shader.hpp"

void Shader::CreateFromSourceFile(const char* vertexSourceFile,const char* fragmentSourceFile)
{
	// Open vertex shader
	std::string vertexSource;
	if(!ReadStringFromFile(vertexSourceFile,vertexSource))
	{
		throw Exception(ExceptionType::ShaderFailedToOpenVertexSourceFile,"Failed to open vertex source file: \v",vertexSource);
	}
	
	// Open fragment shader
	std::string fragmentSource;
	if(!ReadStringFromFile(fragmentSourceFile,fragmentSource))
	{
		throw Exception(ExceptionType::ShaderFailedToOpenFragmentSourceFile,"Failed to open fragment source file: \v",fragmentSourceFile);
	}

	// Load sources from memory
	rethrow(CreateFromSourceMemory(vertexSource.c_str(),fragmentSource.c_str()));
}

void Shader::CreateFromSourceMemory(const char* vertexSource,const char* fragmentSource)
{
	// Create shader Program
	program = glCreateProgram();
	if(!program) throw Exception(ExceptionType::ShaderFailedToCreateProgram,"Failed to create shader program");

	// Load shader stages
	GLint success = 0;
	for (const auto& type : {GL_VERTEX_SHADER,GL_FRAGMENT_SHADER})
	{
		// Create the shader
		GLuint shader = glCreateShader(type);
		if(!shader) throw Exception(ExceptionType::ShaderFailedToCreateShader,"Failed to create shader");

		// Compile shader
		glShaderSource(shader, 1, type == GL_VERTEX_SHADER ? &vertexSource : &fragmentSource, NULL);
		glCompileShader(shader);

		// Check for compile errors
		glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			GLint logLen;
			glGetShaderiv(shader,GL_INFO_LOG_LENGTH,&logLen);
			std::string infoLog;
			infoLog.resize(logLen);
			glGetShaderInfoLog(shader, logLen, NULL, infoLog.data());
			glDeleteShader(shader);
			Destroy();
			switch(type)
			{
				case GL_VERTEX_SHADER:		throw Exception(ExceptionType::ShaderFailedToCompileVertexShader ,"Failed to compile Vertex shader:\n\v",infoLog);
				case GL_FRAGMENT_SHADER:	throw Exception(ExceptionType::ShaderFailedToCompileFragmentShader,"Failed to compile Fragment shader:\n\v",infoLog);
			}        
		}

		// Attach shader to the program
		glAttachShader(program, shader);
		glDeleteShader(shader);
	}
	
	// Link the program with the stages
	glLinkProgram(program);

	// Check for linker errors
	glGetProgramiv(program, GL_LINK_STATUS, &success);
	if (!success)
	{
		GLint logLen;
		glGetProgramiv(program,GL_INFO_LOG_LENGTH,&logLen);
		std::string infoLog;
		infoLog.resize(logLen);
		glGetProgramInfoLog(program, logLen, NULL, infoLog.data());
		throw Exception(ExceptionType::ShaderFailedToLinkShaderProgram,"Failed to link shader program:\n\v",infoLog);
	}
}