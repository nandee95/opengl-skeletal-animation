#pragma once

#include <vector>

#include <glm/vec2.hpp>
#include <freetype2/ft2build.h>
#include FT_FREETYPE_H

#include "Resource.hpp"
#include "Exception.hpp"

REGISTER_EXCEPTION(FontFailedToInitFreetype)
REGISTER_EXCEPTION(FontFailedToLoadFace)
REGISTER_EXCEPTION(FontFailedToLoadGlyph)

#define FontResource Font,FontInfo 

class FontInfo : public ResourceInfo
{
	std::string filename="";
	glm::ivec2 faceDimensions;
public:
	FontInfo(const std::string& filename,const glm::ivec2& faceDimensions = {0,64}) :
		filename(filename),
		faceDimensions(faceDimensions)
	{

	}
	virtual ResourceHash CalcHash() const
	{
		return
			CombineHash(
				CombineHash(
					std::hash<std::string>()(filename),
					std::hash<int32_t>()(faceDimensions.x)
				),
				std::hash<int32_t>()(faceDimensions.y)
			);
	}

	inline const std::string& GetFilename() const noexcept
	{
		return filename;
	}

	inline const glm::ivec2& GetFaceDimensions() const
	{
		return faceDimensions;
	}

	inline void SetFaceDimensions(const glm::ivec2& faceDimensions)
	{
		this->faceDimensions = faceDimensions;
	}
};

class Text;

class Font : public Resource
{
protected:
	friend Text;
	const glm::vec2 internalTextureSize{1024.f,1024.f};
	static constexpr const uint32_t fontSpacing = 8;
	GLuint texture=0;

	struct Character
	{
		glm::vec2 texOffset,texSize;
		glm::vec2 vtxBearing,vtxSize;
		float advance;
	};

	std::vector<Character> characters;

public:

	~Font()
	{
		Destroy();
	}

	virtual void Load(const void* info)
	{
		const FontInfo* fontInfo = reinterpret_cast<const FontInfo*>(info);
		FT_Library ft;
		if(FT_Init_FreeType(&ft))
		{
			throw Exception(ExceptionType::FontFailedToInitFreetype,"Failed to init freetype");
		}

		FT_Face face;
		if(FT_New_Face(ft,fontInfo->GetFilename().c_str(),0,&face))
		{
			FT_Done_FreeType(ft);
			throw Exception(ExceptionType::FontFailedToLoadFace,"Failed to load face from file: \v",fontInfo->GetFilename());
		}

		FT_Set_Pixel_Sizes(face, fontInfo->GetFaceDimensions().x, fontInfo->GetFaceDimensions().y);

		characters.resize(128);

		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(GL_TEXTURE_2D,0,GL_RED,static_cast<GLuint>(internalTextureSize.x),
			static_cast<GLuint>(internalTextureSize.y),0,GL_RED,GL_UNSIGNED_BYTE,nullptr);
		
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

		{
			GLfloat black = 0.0f;
			glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, &black);
			glClearTexImage(texture, 0, GL_RED, GL_UNSIGNED_BYTE, &black);
		}
	
		glm::vec2 offset {fontSpacing,fontSpacing};
		int32_t maxHeight=0;

		for(uint8_t c = 0;c<128;c++)
		{
			if(FT_Load_Char(face, c, FT_LOAD_RENDER))
			{
				throw Exception(ExceptionType::FontFailedToLoadGlyph,"Failed to glyph: \v(#\v)",c,static_cast<int>(c));
			}

			if(offset.x + face->glyph->bitmap.width + fontSpacing > internalTextureSize.x)
			{
				offset.x = fontSpacing;
				offset.y += maxHeight + fontSpacing;
				maxHeight= 0;
			}

			if(offset.y + face->glyph->bitmap.rows + fontSpacing >= internalTextureSize.y) break;

			glTexSubImage2D(GL_TEXTURE_2D,0,offset.x,offset.y,
				face->glyph->bitmap.width,face->glyph->bitmap.rows,GL_RED,GL_UNSIGNED_BYTE,
				face->glyph->bitmap.buffer);

			characters[c] = {
				offset / internalTextureSize,
				glm::vec2(face->glyph->bitmap.width,face->glyph->bitmap.rows) / internalTextureSize,
				{face->glyph->bitmap_left, face->glyph->bitmap_top},
				{face->glyph->bitmap.width,face->glyph->bitmap.rows},
				static_cast<float>(face->glyph->advance.x) / 64.f
			};
			maxHeight = std::max<int32_t>(maxHeight,face->glyph->bitmap.rows);
			offset.x += face->glyph->bitmap.width + fontSpacing;
		}

		FT_Done_Face(face);
		FT_Done_FreeType(ft);
	}

	const Character& GetCharacter(const uint8_t& ch)
	{
		return characters[ch];
	} 

	void BindTexture()
	{
		glBindTexture(GL_TEXTURE_2D,texture);
	}

	void Destroy()
	{
		if(characters.size() > 0)
			characters.clear();

		if(texture != 0)
		{
			glDeleteTextures(1,&texture);
			texture = 0;
		}
	}
};