#pragma once

#include <GL/glew.h>

class UniformBuffer
{
protected:
    GLuint UBO = 0;
public:
    UniformBuffer()
    {
    }

    inline void Create(const void* data,const size_t& size, const GLenum& updateRate = GL_STREAM_DRAW) noexcept
    {
        glCreateBuffers(1,&UBO);
        glBindBuffer(GL_UNIFORM_BUFFER,UBO);
        glBufferData(GL_UNIFORM_BUFFER, size, data, updateRate);
    }

    inline void BindBufferBase(GLuint bindingPoint) noexcept
    {
        glBindBufferBase(GL_UNIFORM_BUFFER, bindingPoint, UBO); 
    }

    inline void Upload(const void* data,const size_t& offset, const size_t& size) noexcept
    {
        glBindBuffer(GL_UNIFORM_BUFFER, UBO);
        glBufferSubData(GL_UNIFORM_BUFFER, offset, size, data); 
    }

    ~UniformBuffer()
    {
        Destroy();
    }

    inline void Destroy() noexcept
    {
        if(UBO)
        {
            glDeleteBuffers(1,&UBO);
            UBO = 0;
        }
    }

};