#pragma once

#include "Entity.hpp"
#include "GL/glew.h"
#include "VertexArray.hpp"
#include "ResourceManager.hpp"
#include "Shader.hpp"
#include <vector>

class Grid : public Entity, public Colorable
{
	VertexArray vao;
	std::shared_ptr<Shader> shader;
	uint32_t vtxCount;
	glm::vec4 oldColor = color;

public:
	bool Create(ResourceManager& resmg,const glm::vec2& xRange,const glm::vec2& yRange,const float& zPos=0.f,const float& spacing=100.f)
	{
		color = {1,1,1,1};
		std::vector<glm::vec3> vertices;
		const glm::vec2 xBarriers =xRange*spacing;
		const glm::vec2 yBarriers =yRange*spacing;

		for(int x = xBarriers.x;x <= xBarriers.y;x+=spacing)
		{
			vertices.push_back({x,yBarriers.x,zPos});
			vertices.push_back({x,yBarriers.y,zPos});
		}
		for(int y = yBarriers.x;y <= yBarriers.y;y+=spacing)
		{
			vertices.push_back({xBarriers.x,y,zPos});
			vertices.push_back({xBarriers.y,y,zPos});
		}
		vtxCount = vertices.size();

		vao.FillVertices(vertices);
		vao.SetAttribPointers({{3,GL_FLOAT}});
	   
		rethrow(shader = resmg.LoadResource<ShaderResource>({"../Shaders/Grid.vert","../Shaders/Grid.frag",ShaderSourceType::SourceFile}));
		
		return true;
	}

	virtual void Update() override
	{
		if(oldColor != color)
		{
			oldColor = color;
			shader->Use();
			shader->SetUniform(0,color);
		}
	}

	virtual void Draw() const
	{
		shader->Use();
		vao.DrawArrays(GL_LINES,0,vtxCount);
	}
};