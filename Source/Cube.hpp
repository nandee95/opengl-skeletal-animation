#pragma once
#include "Entity.hpp"
#include "VertexArray.hpp"
#include "Texture.hpp"
#include "Shader.hpp"

class Cube :  public Entity, public Transformable
{
	struct Vertex
	{
		glm::vec3 position;
		glm::vec2 uv;
	};
	VertexArray vao;
	Shader shader;
	Texture2D texture;
public:
	void Create()
	{
		const Vertex cubeVert[]={
			{{0.f,0.f,0.f},{0.0f,0.0f}},
			{{1.f,1.f,0.f},{1.0f,1.0f}},
			{{1.f,0.f,0.f},{1.0f,0.0f}},
			{{1.f,1.f,0.f},{1.0f,1.0f}},
			{{0.f,0.f,0.f},{0.0f,0.0f}},
			{{0.f,1.f,0.f},{0.0f,1.0f}},

			{{0.f,0.f,1.f},{0.0f,0.0f}},
			{{1.f,0.f,1.f},{1.0f,0.0f}},
			{{1.f,1.f,1.f},{1.0f,1.0f}},
			{{1.f,1.f,1.f},{1.0f,1.0f}},
			{{0.f,1.f,1.f},{0.0f,1.0f}},
			{{0.f,0.f,1.f},{0.0f,0.0f}},

			{{0.f,1.f,1.f},{1.0f,0.0f}},
			{{0.f,1.f,0.f},{1.0f,1.0f}},
			{{0.f,0.f,0.f},{0.0f,1.0f}},
			{{0.f,0.f,0.f},{0.0f,1.0f}},
			{{0.f,0.f,1.f},{0.0f,0.0f}},
			{{0.f,1.f,1.f},{1.0f,0.0f}},

			{{1.f,1.f,1.f},{1.0f,0.0f}},
			{{1.f,0.f,0.f},{0.0f,1.0f}},
			{{1.f,1.f,0.f},{1.0f,1.0f}},
			{{1.f,0.f,0.f},{0.0f,1.0f}},
			{{1.f,1.f,1.f},{1.0f,0.0f}},
			{{1.f,0.f,1.f},{0.0f,0.0f}},

			{{0.f,0.f,0.f},{0.0f,1.0f}},
			{{1.f,0.f,0.f},{1.0f,1.0f}},
			{{1.f,0.f,1.f},{1.0f,0.0f}},
			{{1.f,0.f,1.f},{1.0f,0.0f}},
			{{0.f,0.f,1.f},{0.0f,0.0f}},
			{{0.f,0.f,0.f},{0.0f,1.0f}},

			{{0.f,1.f,0.f},{0.0f,1.0f}},
			{{1.f,1.f,1.f},{1.0f,0.0f}},
			{{1.f,1.f,0.f},{1.0f,1.0f}},
			{{1.f,1.f,1.f},{1.0f,0.0f}},
			{{0.f,1.f,0.f},{0.0f,1.0f}},
			{{0.f,1.f,1.f},{0.0f,0.0f}},
		};

		vao.FillVertices(cubeVert,sizeof(cubeVert));
		vao.SetAttribPointers({{3,GL_FLOAT},{2,GL_FLOAT}});

		rethrow(shader.CreateFromSourceFile("../Shaders/Cube.vert","../Shaders/Cube.frag"));
		rethrow(texture.CreateFromFile("../Resources/cube_diffuse.png"));
	}

	virtual void Update()
	{
		shader.Use();
		shader.SetUniform(0,info.proj);
		shader.SetUniform(1,info.view);
		UpdateModelMatrix();
		shader.SetUniform(2,model);
		shader.SetUniform(3,0);
	}

	virtual void Draw() const
	{
		shader.Use();
		glActiveTexture(GL_TEXTURE0);
		texture.Bind();
		vao.DrawArrays(GL_TRIANGLES,0,36);
	}
};