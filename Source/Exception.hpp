#pragma once
#include <string>
#include <sstream>

#include "Meta.hpp"

#define REGISTER_EXCEPTION(name) // Preprocessed by ExceptionTable.cmake

#define iffails(command,var) try { command; } catch(_Exception& var)
#define rethrow(command) try { command; } catch(_Exception& e) { throw e; }

#define Exception(...) \
_Exception::Create(FIRST_ARG(__VA_ARGS__),FIRST_ARG(EXCEPT_FIRST_ARG(__VA_ARGS__))); \
static_assert(FIRST_ARG(__VA_ARGS__) > ExceptionType::_ExceptionTypeUnknown || FIRST_ARG(__VA_ARGS__) < ExceptionType::_ExceptionTypeMax, "A valid exception type must be specified"); \
static_assert(Meta::CountCharacters(SECOND_ARG(__VA_ARGS__),Meta::VarKey) == ARG_COUNT(EXCEPT_FIRST_TWO_ARG(__VA_ARGS__)),"Formatting doesn't match the variable count");

enum class ExceptionType : uint32_t
{
    _ExceptionTypeUnknown,
    NotImplemented,
    #include "Generated/ExceptionTable.enum"
    _ExceptionTypeMax
};

class _Exception
{
private:
    static constexpr const char keyChar = '\v';
    
    const std::string message;
    const ExceptionType type=ExceptionType::_ExceptionTypeUnknown;
protected:
    _Exception(const ExceptionType& type, const std::string& message) : type(type),message(message)
    {

    }
public:
    static _Exception Create(const ExceptionType& type,const char* message)
    {
        return _Exception(type,message);
    }

    template<typename... Args>
    static _Exception Create(const ExceptionType& type,std::string message,const Args&... args)
    {
        ProcessArgs(message,args...);
        return _Exception(type,message);
    }

	template<typename T,typename... Args>
    static void ProcessArgs(std::string& message,const T& first, const Args&... args)
    {
        std::stringstream ss;
        ss << first;
        message.replace(message.find(Meta::VarKey),1,ss.str());
        ProcessArgs(message,args...);
    }

    template<typename T>
    static void ProcessArgs(std::string& message,const T& last)
    {
        std::stringstream ss;
        ss << last;
        message.replace(message.find(Meta::VarKey),1,ss.str());
    }

    inline const std::string& GetMessage() const
    {
        return message;
    }

    inline const ExceptionType& GetType() const
    {
        return type;
    }
};