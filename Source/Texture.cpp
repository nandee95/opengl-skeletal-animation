#include "Texture.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>

#include <iostream> // TODO

void Texture2D::Load(const void* info)
{
    const TextureInfo* texInfo = reinterpret_cast<const TextureInfo*>(info);

    switch(texInfo->type)
    {
        case TextureSourceType::CompressedFile:
        {
            rethrow(CreateFromFile(texInfo->source));
            break;
        }
        case TextureSourceType::RawMemory:
        {
            rethrow(Create(texInfo->resolution,texInfo->source,texInfo->texFormat,texInfo->texType));
            break;
        }
        case TextureSourceType::CompressedMemory:
        {
            rethrow(CreateFromMemory(texInfo->source,texInfo->size,texInfo->alpha));
            break;
        }
        default:
        {
            throw Exception(ExceptionType::TextureInvalidSourceType,"Invalid texture source type: \v",static_cast<int32_t>(texInfo->type));
        }
    }
}

void Texture2D::CreateFromFile(const char* filename)
{
    int32_t channels=0;
    glm::ivec2 imgRes;
    void* data=stbi_load(filename,&imgRes.x,&imgRes.y,&channels,4);
    
    if(!data)
        throw Exception(ExceptionType::TextureFailedToOpenFile,"Failed to load texture from file: \v",filename);
    
    iffails(Create(imgRes,data,GL_RGB, GL_RGB,GL_UNSIGNED_BYTE),e)
    {
        stbi_image_free(data);
        throw e;
    }
    stbi_image_free(data);
}

void Texture2D::CreateFromMemory(const void* compressedData,const size_t& size,const bool& alpha)
{
    int channels=0;
    glm::ivec2 imgRes;
    void* data=stbi_load_from_memory(reinterpret_cast<const stbi_uc*>(compressedData),size,&imgRes.x,&imgRes.y,&channels,alpha ? 4 : 3);
    
    if(!data)
        throw Exception(ExceptionType::TextureFailedToOpenFile,"Failed to load texture from memory!");
    
    iffails(Create(imgRes,data,GL_RGBA,alpha ? GL_RGBA : GL_RGB,GL_UNSIGNED_BYTE),e)
    {
        stbi_image_free(data);
        throw e;
    }
    stbi_image_free(data);
}

void Texture2D::Create(const glm::uvec2& resolution,const void* data,const GLenum& internalFormat, const GLenum& format,const GLenum& type)
{
    this->resolution = resolution;
    glGenTextures(1,&texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, resolution.x, resolution.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
}

void Texture2D::Destroy() noexcept
{
    if(texture)
    {
        glDeleteTextures(1,&texture);
        texture = 0;
        resolution = {0,0};
    }
}
